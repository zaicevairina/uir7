from flask import Flask, request, redirect, url_for, g
from flask import render_template
from flask import send_from_directory
from werkzeug.utils import secure_filename
from rake import RAKE
from annotation import TextRank
import os
import csv
import chardet
UPLOAD_FOLDER = './media'
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

ALLOWED_EXTENSIONS = set(['csv'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

value = ''
text = ''
annotation = ''
keywords = ''
words_number = ''
approach_of_extract = ''
r = RAKE()
annot = TextRank()

@app.route('/',methods=['GET','POST'])
def start():

    global value
    global text
    global annotation
    global keywords
    global words_number
    global approach_of_extract
    global r

    if request.method=='POST':

        if 'TEXT' in request.form:
            text = request.form['TEXT']
        # прописать предупреждения если что то ввел или ввел непраивльно
        if ('btn' in request.form):

            keywords = r.keywords_extract(text)
            keywords = ', '.join(keywords).capitalize()
            # if 'method_kw' in request.form:
            #     approach_of_extract = request.form['method_kw']
            # else:
            #     approach_of_extract = 'none'
            # if approach_of_extract == 'graph':
            #     keywords = r.rake(text)
            # elif approach_of_extract == 'neiro':
            #     keywords = ''
            # else:
            #     keywords = r.rake(text)
            annotation=annot.extract(text)

        if ('files_work' in request.form):
            file = request.files['file']
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                return redirect(url_for('csv_worker',
                                    filename=filename))
            else:
                return 'Файл не был загружен'

            # всплывающее окно выберите метод

            words_number = request.form['key_words']
            return render_template('main.html',keywords=keywords, annotation=annotation,text=text, words_number=words_number)

    return render_template('main.html',value=value,text=text,keywords=keywords, annotation=annotation, words_number=words_number)

@app.route('/info',methods=['GET','POST'])
def info():
    return render_template('ready.html')

@app.route('/csv/<filename>', methods=['GET', 'POST'])
def csv_worker(filename):
    encoding = [
        'utf-8',
        'cp500',
        'utf-16',
        'GBK',
        'windows-1251',
        'ASCII',
        'US-ASCII',
        'Big5'
    ]
    if request.method == 'POST':
        csv_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        with open(csv_path, "w", newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            line = []
            for some in request.form:
                line.append(request.form[some])
                if len(line) == 3:
                    writer.writerow(line)
                    line = []
        with open(csv_path, "r") as f_obj:
            reader = csv.reader(f_obj)
            return render_template('csv.html', csvs=reader, filename=filename)
    else:
        csv_path_read = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        # открыть csv_path2 изменить и записать изменения
        csv_path = os.path.join(app.config['UPLOAD_FOLDER'], 'result.csv')
        with open(csv_path, "w", newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=';')
            with open(csv_path_read, "r") as f:
                reader = csv.reader(f)
                for row in reader:
                    kw = r.keywords_extract(row[0])
                    kw = ', '.join(kw)
                    an = annot.extract(row[0])
                    writer.writerow([row[0], an, kw])
        with open(csv_path, "r") as f_obj:
            reader = csv.reader(f_obj,delimiter=';')
            return render_template('csv.html', csvs=reader, filename='result.csv')

if __name__=='__main__':
    app.run(debug=True)