from google.oauth2 import service_account
from google.cloud import bigquery
import pandas as pd

project_id = 'arctic-task-238719'
private_key='arctic-task-238719-e6a1c5fe056b.json'

id_article='tyt id_article'
authors='tyt authors'
name_article ='name_article'
article = 'article'
annotation = '3'
keywords = '4'
#
#
# def upload(id_article, authors, name_article,article,annotation,keywords):
#     gr = pd.DataFrame(columns=['id_article', 'authors', 'name_article','article','annotation','keywords'])
#     gr.loc[0] = [id_article, authors, name_article,article,annotation,keywords]
#     gr.to_gbq('db_articles.saved_articles', project_id=project_id, if_exists='append', private_key=private_key)



def upload(id,authors, name_article,article,annotation,keywords):
    gr = pd.DataFrame(columns=['index','authors', 'name_article','article','annotation','keywords'])
    gr.loc[0] = [id,authors, name_article,article,annotation,keywords]
    gr.to_gbq('db_articles.saved_articles_with_index', project_id=project_id, if_exists='append', private_key=private_key)



def index(text,kw,annot):
    x = str(hash(text)+hash(kw)+hash(annot)).replace('-','')
    if len(x)<=6:
        return '0'*(6- len(x))+x
    else:
        return str(int(x)%1_000_000)

