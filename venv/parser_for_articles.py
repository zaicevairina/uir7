# import fitz
# import pandas as pd
# from google.oauth2 import service_account
# from google.cloud import bigquery
# import requests
# import shutil
# from bs4 import BeautifulSoup
# import csv
#
# project_id = 'arctic-task-238719'
# private_key='arctic-task-238719-e6a1c5fe056b.json'
#
# month = ['ЯНВАРЬ','ФЕВРАЛЬ','МАРТ','АПРЕЛЬ','МАЙ','ИЮНЬ','ИЮЛЬ','АВГУСТ','СЕНТЯБРЬ','НОЯБРЬ','ДЕКАБРЬ']
#
# page = requests.get('http://www.tpinauka.ru/%d0%b0%d1%80%d1%85%d0%b8%d0%b2-%d0%bd%d0%be%d0%bc%d0%b5%d1%80%d0%be%d0%b2/')
# soup = BeautifulSoup(page.text, 'lxml')
# table = soup.select_one(".posts")
# refs = table('a')
#
# for ref in refs:
#     if ref.text != 'Журнал одним файлом' and ref.text != 'РИНЦ (постатейно)' and ref.text.find('2016') != -1:
#         page = requests.get(ref['href'])
#         soup = BeautifulSoup(page.text, 'lxml')
#         posts = soup.select_one(".posts")
#         pdfs = posts('a')
#         for pdf in pdfs:
#             title = pdf.text[:pdf.text.find('(')]
#             author = pdf.text[pdf.text.find('(')+1:pdf.text.find('(')+pdf.text.find(')')]
#             print(pdf.text)
#             journal = requests.get(pdf['href'], stream = True)
#             with open("journal.pdf", "wb") as receive:
#                 shutil.copyfileobj(journal.raw, receive)
#
#             # ffff
#             pdf_document = "journal.pdf"
#             doc = fitz.open(pdf_document)
#             text = ''
#             for number_page in range(doc.pageCount):
#                 text += doc.loadPage(number_page).getText("text")
#             # text = text.replace('Международный научно-технический журнал', '')
#             # text = text.replace('«ТЕОРИЯ. ПРАКТИКА. ИННОВАЦИИ»', '')
#
#             for m in month:
#                 text = text.replace(m,'')
#             text = text.replace('2016','')
#             text = text.replace('УДК', '')
#             k = text.find('Ключевые слова: ') + len('Ключевые слова: ')
#             keywords = ''
#             while text[k] != '.':
#                 if text[k] != '\n':
#                     keywords += text[k]
#                 k += 1
#             with open('articles2019.csv', "a", newline="",encoding='utf-8') as file:
#                 article = [title,author,text, keywords]
#                 writer = csv.writer(file)
#                 writer.writerow(article)
#             del journal
#
import requests
import shutil
from bs4 import BeautifulSoup
from time import sleep
import csv
page = requests.get('http://ntv-brgu.ru/arhiv')
soup = BeautifulSoup(page.text, 'lxml')
table = soup.select_one("table")
refs = table('a')
import fitz
for ref in refs:
    if ref['href'].find('2018') != -1:

        print(ref['href'])
        page = requests.get(ref['href'])
        soup = BeautifulSoup(page.text, 'lxml')
        table = soup.select_one(".entry")
        journals = table('a')
        for journal in journals:
            try:
                if journal.text != 'Скачать номер полностью (pdf-файл)' and journal['href'] != 'http://www.famous-scientists.ru/14140' and journal.text.find('Патент') == -1:
                    sleep(1)
                    page = requests.get(journal['href'])
                    soup = BeautifulSoup(page.text, 'lxml')
                    table = soup.find("div", {"id": "content"})
                    print(table.find("a")['href'])
                    page = table.find("a")
                    if page['href'] != 'http://www.ntv-brgu.ru' :
                        print(page['href'])
                        print(page.text)
                        page = requests.get(page['href'], stream = True)
                        with open("journal.pdf", "wb") as receive:
                            shutil.copyfileobj(page.raw, receive)
                    pdf_document = "journal.pdf"
                    doc = fitz.open(pdf_document)
                    text = ''
                    for number_page in range(doc.pageCount):
                        text += doc.loadPage(number_page).getText("text")
                    k = text.find('Ключевые слова: ') + len('Ключевые слова: ')
                    keywords = ''
                    print(k)
                    if k!=-1:
                        while text[k] != '.':
                            if text[k] != '\n' and text[k]!='-' and text[k]!='\t':
                                keywords += text[k]
                            k += 1
                    print(keywords)
                    annotation = ''
                    k = text.find('Ключевые слова: ')-2
                    if k!=-1:
                        while text[k]!='\n' or text[k-1]!=' ' or text[k-2]!='\n' or text[k-3]!=' ':
                            if text[k]!='\n' and text[k]!='-':

                                annotation += text[k]
                            k-=1
                    annotation = annotation[::-1]
                    print(annotation)
                    print(text)
                    if text!='' and annotation!='' and keywords!='':
                        with open('articles_final.csv', "a", newline="", encoding='utf-8') as file:
                            article = [keywords, annotation, text]
                            writer = csv.writer(file)
                            writer.writerow(article)
                        del journal
            except:
                pass