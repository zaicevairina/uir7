#-*-coding: utf-*-
y ="""Различия между данными методами состоят в особенностях учета значимости каждой вершины и вычисления отношений между ними. В данной работе буду рассматривать два алгоритма TextRank и RAKE. При данном подходе фрагменты не обрабатываются, а извлекаются в том порядке и виде, в каком они приведены в тексте. Отбирается 10 слов, которые встречаются в данном фрагменте наиболее часто. Таким образом, наибольший вес получали те фрагменты, которые кроме того, что содержали наибольшее число слов названия статьи, но и большее количество слов часто встречающихся в документе."""
z = '''В данном разделе проводится анализ предметной области, вводятся основные термины, указываются области, в которых могут быть использованы  системы автоматического аннотирования и извлечения ключевых слов. Приводится классификация различных методов автоматического извлечения ключевых слов, дается краткое описание каждого из методов. Рассматриваются два алгоритма TextRank и RAKE, указываются их преимущества и недостатки. Кроме этого приводится классификация различных методов автоматического аннотирования научных статей. Также рассматривается несколько алгоритмов, приводится их краткое описание, указываются их преимущества и недостатки. В конце раздела ставятся цели и задачи учебно-исследовательской работы, и делается вывод о проделанной работе.'''
import numpy
from rouge import rouge_score
import csv
import sys
import rake
from summa import keywords


count = 0
precision = 0
recall =0
import annotation
with open('articles_final.csv', "r",encoding='utf-8') as file:
    reader = csv.reader(file)
    for row in reader:
        text = row[2]
        keyword_true = row[1]
        keyword_pred = annotation.extract(text)
        # print(keyword_true)
        # print(keyword_pred)
        # print(keyword_true.intersection(keyword_pred))
        # print(100*len(keyword_true.intersection(keyword_pred))/len(keyword_pred))

        x = rouge_score.rouge_n(keyword_true, keyword_pred)
        p = x['p']
        r = x['r']
        f = x['f']
        precision+= p
        recall += r
        count += 1
        print(count)
        if count == 100:
            break


print('RESULT')
print('mean precision',precision/count)
print('mean recall',int(recall)/int(count))
print('f-мера',2*precision*recall/count/(precision+recall))
print(count)

